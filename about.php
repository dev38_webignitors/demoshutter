<?php
include('header.php')
?>

<br><br><br><br>


<div>
    <div class="section-title d-flex align-items-center justify-content-center">
        <h1>About us </h1>
    </div><br><br>
    <p class="text-center fs-2 content">
        "We're a highly organised and motivated professional<br>
        photographer with a wealth of experience in a range of <br>
        photographic styles and services."<br>
        <img src="img/sign.gif" alt="" >
    </p>
</div><br><br>
<!-- About services -->
<section class=" top-about about">
    <div class="container ">
        <div class="row row-cols-1 row-cols-md-3 row-cols-lg-3 g-5 ">
            <?php for ($t = 0; $t < 3; $t++) { ?>
                <div class="col ">

                    <div class="card m-auto  border-0 about-contant my-5" style="width: 18rem;">
                        <div class=" mx-auto ">
                            <i class="fas fa-circle-notch fa-lg"></i>
                        </div>
                        <div class="card-body services_card_text">
                            <h5 class="card-title">Natural Photos</h5>
                            <p class="card-text">We are fine-art, wedding & portrait film photographers Oregon, with a special love for natural light, medium format film cameras & redheads with freckles.</p>
                            <a href="#" class=" "><u>Read More</u></a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div><br><br>
</section><br><br><br><br>


<section class="container">
    <h6 class="text-center">Our Team</h6>
    <h2 class="text-center">Photographers</h2>
    <div class="row row-cols-1 row-cols-md-4 row-cols-lg-4 g-4">
        <?php for ($t = 1; $t <= 4; $t++) { ?>
            <div class="col">

                <div class="card mx-auto" style="width: 16rem;">

                    <img src="img/Team/team-<?= $t; ?>.jpg" class="card-img-top" alt="...">

                    <div class="card-body">
                        <h5 class="card-title">Angelina Rose </h5>
                    </div>
                </div>


            </div>
        <?php } ?>
    </div><br><br>
</section>



<section class="testimonials">
    <div class="container">
        <div class="review-carousel owl-carousel owl-theme">
            <?php for ($t = 0; $t < 2; $t++) { ?>

                <div class=" iteam  border-0 p">
                    <div class="m-auto" style="width: 10rem;">
                        <img src="https://picsum.photos/100" class="img" alt="...">
                    </div>

                    <h5 class=" text-center">John Westrock</h5>
                    <p class=" text-center">Shutter is amazing! So creative. But more importantly FUN!. <br>He has a great sense of humor and made our day that much better!! </p>
                </div>



            <?php } ?>
        </div>
    </div>
</section><br><br>

<section class="container">
    <div class="sp-carousel owl-carousel owl-theme">
        <?php for ($t = 1; $t <= 6; $t++) { ?>
            <div class="item">
                <img src="img/sponsor/sponsor-<?= $t ?>.png" class="card-img-top" alt="...">
            </div>

        <?php } ?>
    </div><br><br>
</section>


<?php
include('footer.php')
?>