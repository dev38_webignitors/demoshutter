<?php
include('header.php')
?>
  <!-- Swiper -->

  <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper mySwiper">
    <div class="parallax-bg" data-swiper-parallax="-23%"></div>
    <div class="swiper-wrapper">
      <div class="swiper-slide" style="background-image: url(img/slider/bg-1.jpg);">
        <div class="title" data-swiper-parallax="-300">Smokey Girls </div>
        <div class="subtitle" data-swiper-parallax="-200">Portrait.</div>
      </div>
      <div class="swiper-slide" style="background-image: url(img/slider/bg-2.jpg);">
        <div class="title" data-swiper-parallax="-300">Under Sunshine</div>
        <div class="subtitle" data-swiper-parallax="-200">Portrait.</div>

      </div>
      <div class="swiper-slide" style="background-image: url(img/slider/bg-3.jpg);">
        <div class="title" data-swiper-parallax="-300">Fell The Sky</div>
        <div class="subtitle" data-swiper-parallax="-200">Portrait.</div>

      </div>
    </div>
    <div class="swiper-pagination"></div><br>
  </div>

  <div>
    <div class="container ">
      <div class="profile ">
        <img src="img/p2.jfif" alt="" class="img">
      </div>

      <div class="row ">
        <div class="col-6 border-end border-dark text-end" data-aos="fade-left">
          <h6 class="">ABOUT ME</h6>
          <h2>THOMAS WILLIAM</h2>
          Hey, thanks for stopping by. My name’s Thomas William and Im a Art Director & Photographer based in the San Francisco where I make cool things for agencies and brands around globe. If you like my work then contact me.<br><br>
        </div>
        <div class="col-6" data-aos="fade-right">
          <h6>ABOUT COMPANY</h6>
          <h2>ABOUT SHUTTER</h2>
          Hey, thanks for stopping by. My name’s Thomas William and Im a Art Director & Photographer based in the San Francisco where I make cool things for agencies and brands around globe. If you like my work then contact me.
        </div>
      </div>
    </div>
  </div><br><br>

  <!-- PORTFOLIOS OUR PHOTOGRAPHS -->
  <section class="container ">
    <div class="text-center " data-aos="fade-up">
      <h6>PORTFOLIOS</h6>
      <h1>OUR PHOTOGRAPHS</h1><br><br>
    </div>
    <div class="row row-cols-1 row-cols-md-2 g-4 " data-aos="fade-up" data-aos-anchor-placement="top-bottom">
      <?php for ($i = 0; $i < 9; $i++) { ?>
        <div class="card  text-white col mx-auto border-0" style="width: 18rem;">
          <img src="https://picsum.photos/200" class="card-img" alt="...">
        </div>

      <?php } ?>
    </div><br>
    <div class=" col-2 mx-auto ">
      <button type="button" class="btn btn-dark ">Load more</button>
    </div>
  </section><br><br>

  <section class="testimonial">
    <div class="container">
      <div class="review-carousel owl-carousel owl-theme">
        <?php for ($t = 0; $t < 2; $t++) { ?>
          <div class=" iteam  border-0 p">
                    <div class="m-auto" style="width: 10rem;">
                        <img src="https://picsum.photos/100" class="img" alt="...">
                    </div>

                    <h5 class=" text-center">John Westrock</h5>
                    <p class=" text-center">Shutter is amazing! So creative. But more importantly FUN!. <br>He has a great sense of humor and made our day that much better!! </p>
                </div>


        <?php } ?>
      </div>
    </div>
  </section><br><br>

  <section>
    <div class="parallaxe d-flex flex-column align-items-center justify-content-center ">
      <h5>Shutter Photography</h5>
      <h1>Life is an Adventure <br> Capture Every Minute.</h1>
    </div><br><br>

  </section>
  <!-- blog -->
  <section class="container home">
    <h6 class="text-center">Blog Post</h6>
    <h2 class="text-center">Latest News</h2>
    <div class="row row-cols-1 row-cols-md-3 row-cols-lg-3 g-4">
      <?php for ($b = 1; $b <= 3; $b++) { ?>
        <div class="col">

          <div class="card" style="width: 20rem;">
            <img src="img/blog/blog-<?= $b ?>.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h6>JULY 22,2020</h6>
              <h5 class="card-title">Photo trip to amsterdam</h5>
              <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero, nesciunt rerum? Iusto distinctio libero dolorem aliquam? Minus, officiis temporibus esse mollitia neque autem at aspernatur aliquam! Vitae fugit animi totam.</p>
              <a href="#" >Read More </a>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </section><br><br>

  <section>
    <div class="container">
      <div class="flex ">
        <?php for ($s = 0; $s < 4; $s++) { ?>
          <div><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTahYztqJn-1cjF8_3FztlqjHyyMrQn7AYtkQ&usqp=CAU" alt="" class=" w-100 h-25"></div>&nbsp;
        <?php } ?>
      </div>
    </div>
  </section>

<?php
include('footer.php')
?>