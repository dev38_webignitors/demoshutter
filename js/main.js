$(document).ready(function() {



    $('.review-carousel').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        // autoplay:true,
        // autoplayTimeout:1000,
        // autoplayHoverPause:true
        dots:true,
        // autoplay:true,
        autoplayTimeout:1000, 
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                // nav:true
            },
            600:{
                items:1,
                // nav:false
            },
            1200:{
                items:1
            }
        }  
      
    
    })

    $('.sp-carousel').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        // autoplay:true,
        // autoplayTimeout:1000,
        // autoplayHoverPause:true
        dots:false,
        autoplay:true,
        autoplayTimeout:1000, 
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                // nav:true
            },
            600:{
                items:3,
                // nav:false
            },
            1200:{
                items:6
            }
        }  
      
    
    })
    

});


var swiper = new Swiper(".mySwiper", {
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    speed: 1300,
    parallax: true,
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      
    }
  });

window.addEventListener("scroll", function(){

    var nav = document.querySelector(".navbar");
    nav.classList.toggle("color", window.scrollY > 0)
    var navl = document.querySelector(".navbar-brand");
//     navl.classList.toggle("navb" , window.scrollY > 0)
//     var navl = document.querySelectorAll(".nav-link");
//    navl.forEach( navlinks => {
//     navlinks.classList.toggle("navl" , window.scrollY > 0)
//    });
})



  AOS.init();

