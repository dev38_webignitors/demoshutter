<?php
include('header.php')
?>

<br><br><br><br>
<section>

  <div>
    <div class="section-title d-flex align-items-center justify-content-center">
      <h1>Services</h1>
    </div>

  </div><br><br><br>
  <!-- About services -->
  <section class=" services">
    <div class="row row-cols-1 row-cols-md-3 row-cols-lg-3 g-4 services_container mx-auto">
      <?php for ($t = 1; $t <= 6; $t++) { ?>
        <div class="col">

          <div class="services-card  m-auto border-0 " style="width: 18rem; ">
            <div class="services-card-inner">
              <div class="services-card-front" style="background-image: url(img/services/flip-service-<?= $t ?>.jpg);  background-size: cover;background-position: center;">
                <svg xmlns="http://www.w3.org/2000/svg " width="70" height="90" fill="currentColor" class=" bi bi-camera mx-auto" viewBox="0 0 16 16">
                  <path d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z" />
                  <path d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
                </svg>
                <!-- <img src="https://picsum.photos/100" class="card-img-top" alt="..."> -->

                <h5>Natural Photos</h5>
                <p> "We're a highly organised and motivated professional
                  photographer with a wealth of experience in a range of
                  photographic styles and services.</p>
              </div>

              <div class="services-card-back mt-auto">
                <h5>Natural Photos</h5>
                <p> "We're a highly organised and motivated professional
                  photographer with a wealth of experience in a range of
                  photographic styles and services.</p>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
    </div><br><br>
  </section>



</section>


<section class="testimonials">
  <div class="container">
    <div class="review-carousel owl-carousel owl-theme">
      <?php for ($t = 0; $t < 2; $t++) { ?>

        <div class=" iteam  border-0 p">
          <div class="m-auto" style="width: 10rem;">
            <img src="https://picsum.photos/100" class="img" alt="...">
          </div>

          <h5 class=" text-center">John Westrock</h5>
          <p class=" text-center">Shutter is amazing! So creative. But more importantly FUN!. <br>He has a great sense of humor and made our day that much better!! </p>
        </div>
      <?php } ?>
    </div>
  </div>
</section><br><br>

<section>
  <div class="container">
    <div class="flex">
      <?php for ($c = 0; $c < 4; $c++) { ?>
        <div><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTahYztqJn-1cjF8_3FztlqjHyyMrQn7AYtkQ&usqp=CAU" alt="" class=" w-100 h-25"></div>&nbsp;
      <?php } ?>
    </div>
  </div>
</section>

<?php
include('footer.php')
?>