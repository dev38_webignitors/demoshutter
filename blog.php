<?php
include('header.php')
?>
<br><br><br><br>
<section>
  <div>
    <div class="blog-title d-flex flex-column  align-items-center justify-content-center">
      <h1>Blog</h1>
      <br>
      <p>Build stunning layout of news with Shutter</p>
    </div><br><br>

  </div>
  <section class=" ">
    <div class="container">

      <div class="row ">

        <div class="col-lg-4 entries">
          <?php for ($b = 1; $b <= 3; $b++) { ?>
            <div class="card  border-2" style="width: 18rem;">
              <img src="img/blog/blog-<?= $b ?>.jpg" class="card-img-top" alt="...">
              <div class="card-body ">
                <h5 class="card-title">Natural Photos</h5>
                <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero, nesciunt rerum? Iusto distinctio libero dolorem aliquam? Minus, officiis temporibus esse mollitia neque autem at aspernatur aliquam! Vitae fugit animi totam.</p>
                <a href="#" class="btn btn-primary ">Read More</a>
              </div>
            </div>
            <br>
          <?php } ?>

        </div>

        <div class="col-lg-4 ">
          <?php for ($t = 4; $t <= 6; $t++) { ?>
            <div class="card  border-2" style="width: 18rem;">
              <img src="img/blog/blog-<?= $t ?>.jpg" class="card-img-top" alt="...">
              <div class="card-body ">
                <h5 class="card-title">Natural Photos</h5>
                <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero, nesciunt rerum? Iusto distinctio libero dolorem aliquam? Minus, officiis temporibus esse mollitia neque autem at aspernatur aliquam! Vitae fugit animi totam.</p>
                <a href="#" class="btn btn-primary ">Read More</a>
              </div>
            </div>
            <br>
          <?php } ?>

        </div>

        <div class="col-lg-4">

          <div class="sidebar">

            <input type="text" name="" id="" placeholder="Search"><br><br>

            <h3 class="sidebar-title ">Latest Posts</h3>
            <hr>

            <div class="sidebar-item recent-posts ">

              <div class="">
                <img src="https://picsum.photos/70/50" alt="">
                <span>jun 29, 2021</span>
                <h4 class="">Photo trip to amsterdam</h4>

              </div>

              <div class="">
                <img src="https://picsum.photos/70/50" alt="">
                <span>jun 29, 2021</span>
                <h4>Love together forever</h4>
              </div>

              <div class="">
                <img src="https://picsum.photos/70/50" alt="">
                <span>jun 29, 2021</span>
                <h4>Beautiful white roses</h4>

              </div>

            </div>

            <h3 class="sidebar-title">Categories</h3>
            <hr>
            <div class="sidebar-item p-0">
              <ul>
                <li><a href="#">Nature</a></li>
                <li><a href="#">Portraits</a></li>
                <li><a href="#">Street</a></li>
                <li><a href="#">Studio</a></li>
                <li><a href="#">Weddings</a></li>
              </ul>
            </div>

            <h3 class="sidebar-title">Tags</h3>
            <hr>
            <div class="sidebar-item item-tags ">
              <ul>
                <li><a href="#">camera</a></li>
                <li><a href="#">models</a></li>
                <li><a href="#">nature</a></li>
                <li><a href="#">photography</a></li>
                <li><a href="#">photos</a></li>
                <li><a href="#">shoots</a></li>
                <li><a href="#">studio</a></li>
                <li><a href="#">wedding</a></li>
                <li><a href="#">street</a></li>
              </ul>
            </div>

          </div>

        </div>

      </div>



    </div>




  </section>



  <?php
  include('footer.php')
  ?>