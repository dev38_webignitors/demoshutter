<?php
include('header.php')
?>

<br><br><br><br>
<section>

  <div>
    <div class="section-title d-flex align-items-center justify-content-center">
      <h1>Portfolio</h1>
    </div><br><br>


    <section class="container portfolio">
      <div class="text-center portfolio-head">
        <ul class="d-flex justify-content-center">
          <li> All</li> <br> &nbsp;&nbsp;
          <li>Fashion</li>&nbsp;&nbsp;
          <li>Portrait</li>&nbsp;&nbsp;
          <li>Studio</li>
        </ul>
      </div>
      <div class="row row-cols-1 row-cols-md-3 row-cols-lg-3 g-4 mx-auto">
        <?php for ($i = 1; $i <= 9; $i++) { ?>
          <div class="col">
            <div class="card  text-white col mx-auto border-0" style="width: 18rem;">
              <img src="img/portfolio/pf-<?=$i?>.jpg" class="card-img" alt="...">
            </div>
          </div>
        <?php } ?>
      </div><br>
    </section><br><br>

    <section>
      <div class="container">
        <div class="flex">
          <?php for ($c = 0; $c < 4; $c++) { ?>
            <div><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTahYztqJn-1cjF8_3FztlqjHyyMrQn7AYtkQ&usqp=CAU" alt="" class=" w-100 h-25"></div>&nbsp;
          <?php } ?>
        </div>
      </div>
    </section>


</section>

<?php
include('footer.php')
?>